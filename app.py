import os
from typing import Tuple

import requests
from flask import Flask, request, jsonify
from geopy.geocoders import Nominatim
from flask import Response
from datetime import datetime

from apscheduler.schedulers.background import BackgroundScheduler

app = Flask(__name__)

OPENWEATHERMAP_API_KEY = os.environ.get('OPENWEATHERMAP_API_KEY')
CITY_NAME = os.environ.get('CITY_NAME')

scheduler = BackgroundScheduler()
scheduler.start()

# Define the path to the file for storing temperature data
TEMPERATURE_DATA_FILE = 'temperature_data.txt'


def fetch_and_store_temperature() -> None:
    """
    Fetch and store the current temperature for a specified city.
    This function is scheduled to run periodically.
    """
    try:
        latitude, longitude = get_coordinates(CITY_NAME)
        url = f'https://api.openweathermap.org/data/2.5/weather'
        params = {
            'lat': latitude,
            'lon': longitude,
            'appid': OPENWEATHERMAP_API_KEY,
            'units': 'metric'
        }

        response = requests.get(url, params=params)

        if response.status_code == 200:
            temperature_data = response.json()
            temperature = temperature_data.get('main', {}).get('temp')
            city_temperature = f'Temperature in {CITY_NAME} is {temperature}°C'

            # Append the temperature data to the file
            with open(TEMPERATURE_DATA_FILE, 'a') as file:
                file.write(city_temperature + '\n')
            print(city_temperature)
        else:
            print('Failed to retrieve temperature data')

    except Exception as e:
        print(f'Error: {str(e)}')


scheduler.add_job(fetch_and_store_temperature, 'interval', hours=1)


@app.route('/get_temperature_history', methods=['GET'])
def get_temperature_history() -> Tuple[Response, int]:
    """
    Get historical temperature data for a specified date and city.
    Requires a valid x-token in the request headers for security.
    """
    try:
        day = request.args.get('day')
        x_token = request.headers.get('x-token')

        # Verify the x-token to prevent spam
        if x_token != 'your-secret-token':
            return jsonify({'error': 'Invalid x-token'}), 401

        latitude, longitude = get_coordinates(CITY_NAME)
        dt_unix_timestamp = int(datetime.strptime(day, '%Y-%m-%d').timestamp())

        url = f'https://api.openweathermap.org/data/2.5/onecall/timemachine'
        params = {
            'lat': latitude,
            'lon': longitude,
            'dt': dt_unix_timestamp,
            'appid': OPENWEATHERMAP_API_KEY,
            'units': 'metric'
        }

        response = requests.get(url, params=params)

        if response.status_code == 200:
            temperature_history = response.json()
            return jsonify(temperature_history), 200
        else:
            return jsonify({'error': 'Failed to retrieve temperature history'}), 500
    except Exception as e:
        return jsonify({'error': str(e)}), 400


def get_coordinates(city_name: str) -> Tuple[float, float]:
    """
    Get latitude and longitude coordinates for a specified city.
    """
    geolocator = Nominatim(user_agent="temperature-microservice")
    location = geolocator.geocode(city_name)
    if location:
        return location.latitude, location.longitude
    else:
        raise Exception('Invalid city name')


if __name__ == '__main__':
    app.run(debug=True)
