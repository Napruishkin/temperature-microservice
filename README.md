# Temperature Microservice

This Flask-based microservice fetches and stores current temperature data for a specified city at regular intervals and provides historical temperature data for a given date and city.

## Prerequisites

Before running the microservice, make sure you have the following prerequisites:

- Python 3.x
- Flask
- requests
- geopy
- apscheduler

You also need to obtain an API key from [OpenWeatherMap](https://openweathermap.org/api) and set it as an environment variable (`OPENWEATHERMAP_API_KEY`).

## Getting Started

Clone this repository to your local machine:

```bash
git clone https://gitlab.com/Napruishkin/temperature-microservice.git
cd temperature-microservice
```
   
### Install Required Python Packages

Use the following command to install the required Python packages:

```bash
pip install -r requirements.txt
```

### Set Up Environment Variables

Before running the microservice, you need to configure the following environment variables:

- `OPENWEATHERMAP_API_KEY`: Your OpenWeatherMap API key.
- `CITY_NAME`: The name of the city for which you want to store temperature data.

Make sure to set these variables with their respective values before starting the microservice. These variables are essential for the proper functioning of the temperature microservice.

### Get Historical Temperature Data

Use the following endpoint to get historical temperature data for a specified date:

- **GET /get_temperature_history**

Include the following parameters in the request:

- `day`: The date in the format Y-m-d (e.g., "2023-09-22").
- `x-token`: A secret token for authentication.

**Example CURL Request:**

```bash
curl -X GET -H "x-token: your-secret-token" "http://localhost:5000/get_temperature_history?day=2023-09-22"
```

You can use this endpoint to retrieve historical temperature data for the specified date and city. Ensure you include the x-token header with a valid token for authentication.

### Customizing the Authentication Token

You can customize the authentication token used for the `/get_temperature_history` endpoint by modifying the code. Follow these steps to change the `x-token` value:

1. Open the source code file `app.py` in your preferred text editor.

2. Locate line 71 in the code, which is responsible for verifying the `x-token`.

3. Change the value `'your-secret-token'` to the desired authentication token of your choice.


   ```python
   if x_token != 'my-custom-token':
       return jsonify({'error': 'Invalid x-token'}), 401
   ```

### Start the Microservice
To start the microservice, use the following command:

```bash
python app.py
```

The microservice will be accessible at http://localhost:5000.
